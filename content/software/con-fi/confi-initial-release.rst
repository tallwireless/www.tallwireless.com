Con-Fi: Lightweight Wireless Management for Events
##################################################

:summary: A look into a lightweight wireless for events
:tags: events, software, defcon, blueteamvillage
:date: 2019-10-12 11:33

`Con-Fi`_ is a lightweight wireless management system for events. I wrote it
to support the wifi network for the `Blue Team Village`_ this year at
`DefCon`_. It's based on a wireless registration system for ShmooCon written
by Dragorn. The system provides a JavaScript free registration page for
attendees to register for an 802.1x account.

Working in WiFi as part of my job, I had used systems like PacketFence and
ClearPass to run autentication for the networks I run. These systems require
large systems to run, and are a 16lbs sledge hammer where a push pin is
needed. Thinking back to a conversation with Dragorn at `The WOPR Summit`_, I
figured I'd tackle writting a bare bones wireless management to support BTV
this year.

I was able to get the code done before the event in August, but there are
still a number of things which still need to be completed. I'm hoping to
tackle a number of them before WOPR this year.

Stay tuned for more posts on Con-Fi over the next couple of months as we
approach the start of WOPR.

.. _`Con-Fi`: https://github.com/tallwireless/con-fi
.. _`Blue Team Village`: https://www.blueteamvillage.org
.. _`DefCon`: https://www.defcon.org
.. _`The WOPR Summit`: https://www.woprsummit.org`

Features
********
802.1x Wireless Authentication
++++++++++++++++++++++++++++++
Con-Fi uses WPA2-Enterprise to support authenticating attendees. Using
EAP-PEAP/MS-CHAPv2, users are able to authenticate with the username and
password created during the self registration.

JavaScript-Free Self Registration
++++++++++++++++++++++++++++++++++
As this system was developed to primarily support attendees at hacking
conferences, there are a large number of attendees which don't have JavaScript
turned on inside of their browsers. The registration page is completely
JavaScript free, reducing the barrier to entry to the wifi network.

User and Group RADIUS Attributes (Partially Completed)
++++++++++++++++++++++++++++++++++++++++++++++++++++++
Administrators are able to add RADIUS attributes to both users and groups.
This allows administrators to put different groups into different VLANs or
set wireless roles.

*To be implemented:* Web based management of RADIUS attributes

Docker Build (Partially Completed)
++++++++++++++++++++++++++++++++++
Being able to quickly bring up the system is important. Con-Fi is built on
Docker flexiblity and portablity.

*To be implemented:* Fully automated running of Con-Fi

Future Features
***************
There are a number of different features I would like to get written into
Con-Fi to be able to make deployment and management easier. As time goes on,
I'll be writing more features into Con-Fi. One of the primary features to be
written next is an administrative interface for managing users, groups, and
RADIUS attributes.

