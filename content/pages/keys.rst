Keys
########
:side_weight: 10

I like my encryption. So...here are some public keys of mine.

PGP Key
+++++++

This is my current `pgp key`_.

Current fingerprint is:

.. code-block:: TextLexer

  EA0F 100E B37E 80C8 BF42  BB06 1BA5 F814 173F 5F3A

Retired Fingerprints:

.. code-block::  TextLexer

  57BB 8770 FEBF FCED 546A  6845 72FB 2929 F3D8 215A
  CA5F 3F3E 4CDD A131 B476  E662 FF20 ECFC 8E90 724B


SSH Key
+++++++

This is my current `SSH public key`_.

.. _`SSH public key`: {attach}ssh-charlesr-yubi.pub
.. _`pgp key`: {attach}1BA5F814173F5F3A.gpg
