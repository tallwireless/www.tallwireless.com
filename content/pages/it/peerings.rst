Peerings
========

Because I'm a crazy person, I own an AS and have an IPv6 allocation. The
allocation is mostly used for just dorking around and seeing what fun things
I can do with it. Most of the peerings are with the co-lo facilities I have
servers in or have virtual private servers with. Below is who I peer with:

.. csv-table:: Peerings
    :file: peerings.csv
    :header-rows: 1
    :align: center

The following is my prefix breakdown:

.. csv-table:: IPv6 Allocation Break Down
   :file: ipv6_allocations.csv
   :header-rows: 1
   :align: center
