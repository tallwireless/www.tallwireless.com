Visiting Ireland
================

:status: hidden

Contacting Charles
------------------
Well, you're probably going to want to contact me in some capacity while you are here in Dublin. My
USA number still works here, along with the associated services (Signal and WhatsApp). I also have
a Irish number if you need it (+0858353829). Surprisingly, email also works over here if push comes
to shove.

Flat Address
------------
Adelaide Square, Whitefriar Street, Apt 60, Dublin, Dublin 2


Getting from Airport to Flat
----------------------------
There are a couple of different ways to get from the airport to the flat. The two easiest ones are
bus and taxi.

Bus
+++
If you take the bus, you will need cash to pay for your fare. The fare is €2 per person. The bus
takes about 50 minutes to get to the flat and will drop you off a 5 minute walk from the flat. You
will want to catch the 16 bus to Ballinteer. It's located in zone 13/15. From memory, as you exit
the terminal, and get down to where the taxis are, you are going to want to head to the left and
walk about 3-4 minutes over to the buses. It should the the first zone on your right. (Please
correct me on this if I'm wildly incorrect about this!) You are going to want to get off at the
Whitefriair St stop.

Taxi
++++
If you don't want to go on a search for the bus or manage luggage on the bus, you can grab a taxi to
the flat. It's between €40-€50 to get down to the flat. Cash is recommended, but they should all
take cards.

Ride Share
++++++++++
Lyft is not a thing in Dublin, and I've never tried Uber from the airport. I have only taken taxis
and the bus. Proceed at your own risk! (But do report back so I can update the page!)

When you get here
+++++++++++++++++
Please shoot me a text message when you are getting on your selected mode of transit, and which mode
you are on. I'll either meet you at the bus stop or in front of the building when you arrive.

Staying at the flat
-------------------
While at the flat, you'll have your own room with a Irish Super King bed, a window that opens in two
ways, blackout curtains, towels, and some closet space. So there is no need to bring linens and such
unless you feel the need to have a space something while you visit. There is also plenty of shampoo,
conditioner, and soap to sink a couple of ships in the flat, along with a hair dryer and curling
iron.

You will have one key and fob while you are here to get into the flat complex, along with the flat
itself. They really like electronic locks on exterior doors here, so the fob is definitely needed.
But we can also toss keys out a window if need be.

It is a full flat with a full kitchen and the such. Fridge, kettle, whisks, and everything. We'll
probably cook some meals in the apartment while you're here.

There is a parking spot associated with the flat, so if you plan on renting a car in either May or
June, you are welcome to park it in the spot. In July it will be occupied.

Wireless
++++++++
There are two SSIDs in the flat: LinuxCats and NotIreland. LinuxCats (password: `sophieisthecoolest`) will
drop you an Ireland based Internet, and is probably the best thing to use while visiting. It will be
faster than the other SSID. NotIreland (password: `leprechauns`) will drop you on US based Internet
out of Philadelphia. It will work, but will be slower as EVERYTHING has to be sent across the ocean
first. You probably aren't going to need it, but it's there if you do.

Electronics and outlets
-----------------------
Just remember Ireland is 220v and has the UK plug. Most electronics will very happy switch between
110v and 220v, but you should double check before plugging anything in. I do have a number of extra
plug converters for you if you need them, along with plenty of USB-C power bricks.

I do not have a 220v to 110v transformer, so plan accordingly if you are going to need one.

The Area
--------
The flat is located no more than a 10-15 minute walk from most of the touristy stuff in City
Centre/Temple Bar, and 20 minute walk from the major shopping area in City Center.

There are plenty of restaurants and bars with in 10 minutes of the flat, as well as like 5 grocery
stores and pharmacies Incase you forget something.

The flat is sitting between two major bus arteries, so getting on public transit is super easy. One
of the tram lines (Green Line) is about a 10 minute walk over near St Stephens Green, and is about
a 25 minute walk to Connelly Station. You will need to take a tram (Red Line) or a bus to get to
Heuston Station.

If you are planning on using the buses or the tram lines, I would recommend getting a Leap card,
Dublin's version of a SEPTA Key card. The trams are a tap on/tap off system, and will cost you €1.30
to €2.00 per trip. Bus rides are €2 a trip.

Charles' Schedule
-----------------
Well, sadly these three months aren't vacation for me, so I have to work during the week. My hours
are sorta flexible, and if you would like to me come on an adventure with you, just give me a couple
weeks heads up so I can take off.

Generally, my work hours are:

+------------------+-------------+
| Day              | Times       |
+==================+=============+
| Monday, Thursday | 1230 - 1930 |
+------------------+-------------+
| Tuesday, Friday: | 0900-1700   |
+------------------+-------------+

My Wednesdays alternate between those two hour sets, so just ask which week it is.

As one of the main reasons I came over is for bell ringing, so I have bell ringing at the following times:

+------------------+----------------------+
| Day              | Times                |
+==================+======================+
| Tuesday          | 1900-2000            |
+------------------+----------------------+
| Friday           | 1830-2030            |
+------------------+----------------------+
| Sunday           | 0930-1130, 1430-1530 |
+------------------+----------------------+

One the Wednesdays I start work at 9am, I typically head up to Drogheda for the evening to ring.

You are more than welcome to come along for any of the ringing listed above, and unless practice is
cancelled, this is where you find me during these times.

There are other events and such I will be attending, but they haven't been scheduled yet. I'll give
you a heads up if there is something else scheduled while you are here.

Clothing
--------
Well, you are coming to Ireland, so expect to get wet, so plan accordingly. One thing to note is
a number of restaurants and some bars over here won't let you in if you are wearing shorts. How
classy and discriminatory against those who over heat easily are they! So, if you are heading out
for dinner, probably a good idea to slap some pants on.
