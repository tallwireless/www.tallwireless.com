Contact me
##########
:side_weight: 10000


Contact Information
+++++++++++++++++++

If you are looking to contact me, the best way is by
email_. I also update my twitter_ from time to time.


.. _email: mailto:charlesr@tallwireless.com
.. _twitter: https://www.twitter.com/tallwireless
