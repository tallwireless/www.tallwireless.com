About Me
########

:side_weight: -10

.. image:: {attach}headshot.png
   :width: 25 %
   :align: right
   :alt: headshot

Charles Rumford currently works for `Deft`_ as network engineer on the
operations team. His main responsiblities include routing, network design,
systems administration, network design, and automation. He has a love for IT
infrastructure and ensuring it meets the needs of his users. "Making events
non-events" and "no one should know we've done anything" are his primary guiding
princples for designs and work.  Usiblity is also a primary concern, as what's
the point of building something if no one uses it...

Outside of the work space, Charles doesn't stray to far from the technology
path. Recently, he has joining the planning commitee for the `The WOPR
Summit`_ and the `Blue Team Village`_ @ `DEFCON`_.
His primary roles is to support conference IT infrastructure to ensure a
smooth conference for staff, speakers, and attendees.

When Charles isn't mucking around with strange uses of BGP or protocol
development for a 250 baud wireless backhaul network, he spends a great deal
of time ringing `church tower bells`_, biking, and knitting up a
storm.

Affilated Organizations
+++++++++++++++++++++++
* `Deft`_ - Network Engineer
* `Blue Team Village`_ - Board of Directors Member
* `The WOPR Summit`_ - Planning Committee Member
* `Philadelphia Guild of Change Ringers`_ - Steeple Keeper/Treasurer
* `Morris Animal Refuge`_ - Volunteer

Past Affilated Organizations
++++++++++++++++++++++++++++
* `University of Pennslyvania`_ - IT Architect

.. _`Deft`: https://www.deft.com
.. _`University of Pennslyvania`: https://www.upenn.edu
.. _`Blue Team Village`: https://www.blueteamvillage.org
.. _`The WOPR Summit`: https://www.woprsummit.org
.. _`Philadelphia Guild of Change Ringers`: http://www.phillyringers.com
.. _`Morris Animal Refuge`: https://www.morrisanimalrefuge.org
.. _`DEFCON`: https://www.defcon.org
.. _`church tower bells`: http://nagcr.org/pamphlet.html
