Welcome!
========

:category: General
:date: 2017-04-11 23:36:53

Welcome everyone to my website and blog! I've been meaning to get this site up
and running for a while. Well, here it finally!

aoeuaoue

The goal I had for this site was to serve as a means for me to write about
interesting technical things I'm working on at the moment. This could include
things I'm doing at work, or at home. There could also be some stuff that
leaks in that is non-technical.

I'm hoping to update the blog section of the site at least once a week, if
not more. I hope you come back in the future!


