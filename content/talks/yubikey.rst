YubiKey: More than 2FA
######################

:summary: A talk about what you can do with a YubiKey
:tags: talk, yubikey, dc215
:date: 2019-11-19 18:45

Gottchas:

 * Remember, putting you GPG keys on to your YubiKey is destructive operation
   if you save the state. Make sure to back up your private keys before
   putting the keys on your YubiKey.
 * Backup up your YubiKeys on to a encrypted thumbdrive


Slides_


References:

* `YubiKey Guide`_ - Great guide for putting your GPG private keys onto your YubiKey
* `Ubuntu - U2FA PAM Support`_
* `Ubuntu - Challange/Response PAM Support`_


.. _`YubiKey Guide`: https://github.com/drduh/YubiKey-Guide
.. _`Ubuntu - U2FA PAM Support`: https://support.yubico.com/support/solutions/articles/15000011356-ubuntu-linux-login-guide-u2f
.. _`Ubuntu - Challange/Response PAM Support`: https://support.yubico.com/support/solutions/articles/15000011355-ubuntu-linux-login-guide-challenge-response
.. _Slides: {attach}20191119-yubikey.pdf
