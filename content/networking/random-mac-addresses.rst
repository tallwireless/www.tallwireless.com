Thoughts on Wireless MAC Address Randomization
##############################################

:summary: Some thoughts on the affects of MAC address randomization of mobile devices
:tags: wlan, enduser, usablity, security
:date: 2020-07-15 17:00

----------------------------------------------

Recently, it was announced by Apple that they would be defaulting to
randomizing MAC addresses for iPhone and iPad devices on a per SSID basis, and
rotating them every 24 hours. This change is being done to decrease the
ability to track a device. While the general idea is not something new, it's
starting to be made default on more and more operating systems. Android
recently enabled it by default with the release of Android 10, but without the
additional 24-hour rotation.

As a user of many different mobile wireless devices, I'm totally on board with
the idea of MAC address randomization to increase my privacy as I roam from
coffee shop to coffee shop and bar to bar developing some crazy project. But
the feature introduces some interesting problems to wireless network
operators.

Here are some of my thoughts regarding MAC address randomization becoming more
prevelent in the world. Fair warning: it is pretty basis towards the higher
education space mainly because that's the space I work in.

Here goes.

Use Cases for Wireless
++++++++++++++++++++++
In the general sense, I feel there are three primary use cases or spaces
people use their wireless devices in: home, guest, and enterprise. 

Home
====
Generally, users connecting to their home network don't care about what the
MAC address of the device is. They just click on the SSID for their home
access point, (hopefully) type in the password, and presto they are connected
to the network and able access the Internet and play their Bach organ
concertos through their wireless speakers.

The network doesn't care what the MAC address of the device is, and if it
changes the worst that's going to happen is the IP address is going to change,
but generally, that hasn't affected anyone before. IoT devices generally don't
have MAC address randomization because they are stationary to the house and
don't roam around the city.

Guest
=====
When I talk about the guest use case or space, I'm talking about the guest
wireless offered by establishments. This could be the wifi in a coffee shop or
a restaurant, but it's also in a hotel, conference center, university, or
corporation. The guest user is one that is typically using the serivce
temporarily.

MAC address randomization is generally fine here because it doesn't matter
between visits because you would have to go through the captive portal process
again any way, so it doesn't matter if your device's MAC address changes day
to day.

I'm going to wave my hands a bit at this point regarding guest users and say
they are guests for less than 24 hours. I'll dig a little deeper the issues
around longer term guest in a bit.

Enterprise
==========
The enterprise is a fairly broad space, but the primary factor here is that
wireless device wireless devices are authenicated to wireless networks not by
the MAC address of the device, but with credenital on the device over 802.1x.
The changing of the MAC address doesn't matter to get a device on.


The Problems
++++++++++++
From the above areas, the home wireless space is the one space which doesn't
face any issues with devices randomizing their MAC addresses. The other two
areas have a couple of use cases which don't cover the majority of use cases. 

Long Term Guest Users
=====================
Guest wireless networks have long been deploy using the MAC address of a
device as a method for tracking the device and it's authentication. Along with
that, most guest users don't use a guest networking for more than a couple of
hours, and at most a work day. At my university, we force users to re-enter
their email address once a day. With MAC addresses rotating on a 24 hour
interval, this doesn't cause any problems.

Where the problems start to come into play is around long term guests. This
could be people attending a confernce, or staying in a hotel for multiple
nights. With rotating the MAC addresses of a device, a user will be forced to
run through a captive portal at least once every day. For some, this is
perfectly fine as long as it's once per day per device.

There are some other options out there to help address the long term guest
issue. One of the common solutions to this probelm is to force the user
through some kind of on-boarding process to issue them some kind of
credentials and them have them connect to an 802.1x network. This is an
appealing idea but introduces a barrier for the end user to connect,
ultimately resulting in the reduction of people using the network.

Another solution is PassPoint. PassPoint has been around for a while and aims
at trying ease network roaming issue by using a set of strong credentials that
are portable between SSIDs. Super cool, right? Well, PassPoint makes the
assumption that A) a user has been issued credentials by some kind of identity
service provider and B) the service provider has a relationship with that
identity service provider. Without both, then the user is either out of luck
or has to go through a high barrier onboarding process.  (I plan on writing a
more indepth post about my thoughts on PassPoint. There are many, but
ultimately, I think PassPoint is super cool and has many uses.)

Enterprise IoT
==============
Now, you were probably thinking "What could be wrong in the enterprise space?
Strong individual credentials! Identity based authentication! It's going to be
great!" In that thinking, you wouldn't be wrong. Identity based authentication
that moves away from using the MAC address is great, but there are still many
different devices that either A) don't support 802.1x or B) belong or are used
by multiple people.

Let's first look at the devices not supporting 802.1x. These are your
Playstations, lightbulbs, Echos, Google ChromeCast, and many other devices.
The likely hood these devices will start doing MAC address randomization is
pretty low. They aren't portable. Unless my Chromecast decides to take a gap
year and bum around Europe, there is no concern they will tracked.  These
devices can still take advantage of MAC based registration systems already
deployed today. After the user spends several hours extracting the MAC address
from the device, they simply register it and BAM! the device is connected and
leaking all your private conversations to big tech!

But there are still devices used by multiple people, support 802.1x, and
implement MAC address randomization. These devices could be kisoks, loaner
laptops, and other such devices which might end up being
iOS/iPad/Android/Windows devices. These are devices which might not be owned
by a single person, but by a department or a lab. With the MAC address
randomization, the device's MAC address can't just be registered and magically
it can connect. You have to figure out what the MAC address is for the given
SSID, and then hope it doesn't change on you.

The simple solution to this probelm is to just disable the MAC address
randomization for the SSID on the device so the MAC address becomes
deterministic. I say this is a simple solution, but it requires educating
users they must do it, and then them actually doing it. I generally have low
confidence in this happening. It's just going to create more support issues
for the organization.

The alternative to this is to generate a user agnostic credential and then
onboarding the device on to the 802.1x network. Simple, yeah? Well, this
creates issues with mass device management tools such as machine imagers and
MDM solutions. Managers of devices want to be able to automate the whole thing
out, and unless the credential creation and onboarding can be automated out,
then their workflows are broken.

The other side of onboarding devices via 802.1x is now your IAM system has to
have a way of handling and mananging device based credentials. By policy, some
places only allow crenditals to be created for actual humans. Changes to the
IAM structure of an organization would have to be tackled to be able to
accomidate this. Which could take a while.

Final Thoughts
++++++++++++++
I think the general concept behind MAC address randomization is a great
increase in privacy for end users, but it comes at a price for wireless
network operators and at some level the end user itself.

One of my strongest personal opinions regarding wireless is end users
onboarding is a greatly underrated problem. If the users have to jump through
to many hoops to get on, then they won't. We see this all the time where users
will prefer a deminish network expirence on the guest network rather than go
through the "pain" of onboarding their device.

With the wider deployment of MAC address randomization, there are going to
be changes in how wireless network operators deploy different kinds of
networks. The IT space is going to have to absorb as much of the complexity
created by these changes. This includes changes to backend IAM and
authentication sysetms tha support wireless.

And finally, I'm all for moving away from MAC and IP address based access
control and moving towards identiy based access control.
