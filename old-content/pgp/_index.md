+++
date = "2017-04-11T23:11:53-04:00"
title = "PGP Keys"
draft = false

+++

This is my [pgp key](1BA5F814173F5F3A.gpg).

Current fingerprint is:

    EA0F 100E B37E 80C8 BF42  BB06 1BA5 F814 173F 5F3A

Retired Fingerprints:
    
    57BB 8770 FEBF FCED 546A  6845 72FB 2929 F3D8 215A
    CA5F 3F3E 4CDD A131 B476  E662 FF20 ECFC 8E90 724B 
        Note: This one I lost the private keys on because of a SmartCard
        mishap.


