+++
date = "2017-04-11T21:37:42-04:00"
title = "About Me"
draft = false
+++
<img src="/photo.png" style="display:block; margin-left: auto; margin-right: auto; width: 250px">


Charles Rumford currently works for the [University of
Pennslyvania](http://www.upenn.edu) as IT Archiect for the
Network Engineering, Research and Devlepoment group. His main responsiblities
include wireless networking, routing, network design, systems administration,
and network design. He has a love for IT infrastructure and ensuring it meets
the needs of his users. "Making events non-events" and "no one should know
we've done anything" are his primary guiding princples for designs and work.
Usiblity is also a primary concern, as what's the point of building something
if no one uses it...

Outside of the work space, Charles doesn't stray to far from the technology
path. Recently, he has joining the planning commitee for the [WOPR
Summit](https://www.woprsummit.org) and the [Blue Team
Village](https://www.blueteamvillage.org) @ [DEFCON](https://www.defcon.org).
His primary roles is to support conference IT infrastructure to ensure a
smooth conference for staff, speakers, and attendees.

When Charles isn't mucking around with strange uses of BGP or protocol
development for a 250 baud wireless backhaul network, he spends a great deal
of time ringing [church tower bells](https://wwww.nagcr.org) and knitting up a
storm.
