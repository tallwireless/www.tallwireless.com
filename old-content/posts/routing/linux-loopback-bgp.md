+++
title = "Service Addressing for Linux via BGP"
draft = true
description = "A look at doing service addressing on loopback addresses using BGP to announce the service address"
categories = ["Networking","Routing", "Linux"]
tags = ["bgp","linux","loopback","colo"]
comments = false
date = "2018-10-07T17:05:24-04:00"

+++

Over the last couple of months, I have have been pondering ways to improve
network connectivity to services at work. A number of our services rely on
layer 2 connectivity between servers to handle their high availability. Yuck!

Doing some work on how to move our network forward in design, I started to
think about not just the network design, but also what was connected to the
network and how they leveraged the network. What's the point of building a
new fancy network if you have to keep bring the old desgins along with you?

Around this time, I had seen a couple of postings on the /r/networking
subreddit about operating TCP services with anycast. While there may be some
issues with running say a web application in a small network with anycast
where you might not know where your traffic might end up each time, this was
the inspiration for using the basics of anycast with BGP for handling services
within our network. 

While I haven't been able to build this out at work, I have been able to
build this out using this model for a number of services I run within my own
IT infrastructure. This post will go over the basics of anycast, and ways I
have used it within my network.

# The Setup

<<< Diagram goes here >>>

My home IT infrastructure for this consits of two sites: the colo and my
house. The two sites are connected with an OpenVPN tunnel, and a BGP peering
between the two of them. 

The colo has a PFSense router at the edge. The Internet connection is a static
route to the colo provider over a /30. A /29 of IPv4 space is also routed to
me over the /30. IP addressing with my colo space is all RFC1918 space.

My house has your standard home Verizon FIOS connection to the Internet with a
WAN DHCP address and all. At the border is a PFSense box to handle the BGP and
VPN tunnels. For the purposes of this setup, all traffic from my house's
server network is default routed to the Internet via the colo.

# Setting Up the BGP Route Reflectors
At the heart of this setup are two route reflectors, one at each site. These
will provide a point for the servers to connect to and provide their routes.
For this, I wanted to use some kind of software based BGP software with the
goal of being able to control it programatically in the long term.

I ended up going with [GoBGP](https://osrg.github.io/gobgp/). It can be
controlled programatically via a RPC API, which I haven't done yet. One of the
features I really enjoyed about it is the ablity to list whole subnets as
neighbors. This puts GoBGP into a state passive listening state waiting for
clients to join. Ultimately, individual neighbors will added and removed via
automation, but I'm going to be lazy and just use the subnets for this setup.

Here is the GoBGP configuration for the colo:

    [global.config]
      as = 65301
      router-id = "172.18.129.2"
    [[peer-groups]]
      [peer-groups.config]
        peer-group-name = "asgard"
        peer-as = 65301
      [peer-groups.route-reflector.config]
        route-reflector-client = true
        route-reflector-cluster-id = "172.18.129.1"

    [[neighbors]]
      [neighbors.config]
        neighbor-address = "172.18.129.1"
        peer-group = "asgard"

    [[dynamic-neighbors]]
      [dynamic-neighbors.config]
        prefix = "172.18.129.0/24"
        peer-group = "vms"

# The Server
For the server side, I ended using Quagga/BGPD to interact with the route
reflector. I was running into some issues with GoBGP and announcing routes
from the config file, so I went with BGPD for ease with the intentions of
going back and getting GoBGP working on the server side.

The configuration for BGPD is as follows for one of my name servers:

    router bgp 65301
        bgp router-id 172.18.129.8
        network 76.8.56.26/32

        neighbor 172.18.129.2 remote-as 65301

    !

If we look at the GoBGP server, we see the following entires:

    [charlesr@bgp-colo ~]$ gobgp global rib
       Network              Next Hop             AS_PATH   Age        Attrs
    *> 76.8.56.26/32        172.18.129.8                   6d 23:59:26 [{Origin: i} {Med: 0} {LocalPref: 100}]
    [charlesr@bgp-colo ~]$

and on the PFSense box we see the following:

    flags: * = Valid, > = Selected, I = via IBGP, A = Announced, S = Stale
    origin: i = IGP, e = EGP, ? = Incomplete

    flags destination          gateway          lpref   med aspath origin
    I*>   76.8.56.26/32        172.18.129.8       100     0 i

Now that the network knows how to get the packets heading to 76.8.56.26 to the
nameserver, we have to configure the name server to be able to handle traffic
to that address. For services to be able to use this anycast address, we have
to add it to some kind of interface. Instead of adding it to a physcial
interface, we are going to add it the loopback address:

    charlesr@nimi01 $ sudo ip addr add 76.8.56.26/32 dev lo
    charlesr@nimi01 $ ip addr show lo
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet 76.8.56.26/32 brd 76.8.56.26 scope global lo:0
       valid_lft forever preferred_lft forever

We are now able to ping the anycast address:

    [charlesr@spider ~]$ ping -c3 76.8.56.26
    PING 76.8.56.26 (76.8.56.26) 56(84) bytes of data.
    64 bytes from 76.8.56.26: icmp_seq=1 ttl=64 time=1.39 ms
    64 bytes from 76.8.56.26: icmp_seq=2 ttl=64 time=1.04 ms
    64 bytes from 76.8.56.26: icmp_seq=3 ttl=64 time=1.31 ms
    
    --- 76.8.56.26 ping statistics ---
    3 packets transmitted, 3 received, 0% packet loss, time 2003ms
    rtt min/avg/max/mdev = 1.045/1.252/1.394/0.149 ms
    [charlesr@spider ~]$

# Directing Traffic
In a classical anycast environment, the decison on which direction the traffic
goes is dependent  on where the routes live with in the network. This works
really well for services like DNS or NTP where the state of the connection
isn't as important. For services like HTTP or SSH, having your connection
consistently go to the same place is important.

To build a service which uses the classic active/passive model, we must
direct all the traffic to a single server. To do this, I leveraged the
Multi-Exit Discriminator \(MED\) attribute within BGP. The MED attribute is
used as part of the BGP route selection process to determine the best route.
Given two routes with identical attributes, the route with the lowest MED
wins. This happens before checking things like IGP metric.

Looking back at my setup, I added a recursive DNS service IP address to both
of my name servers. Looking at the route reflector, you can see there are two
routes to 172.18.255.1:

	[charlesr@bgp-colo ~]$ gobgp global rib
	   Network              Next Hop             AS_PATH   Age        Attrs
	*> 172.18.255.1/32      172.18.129.8                   00:00:01   [{Origin: i} {Med: 0} {LocalPref: 100}]
	*  172.18.255.1/32      172.18.129.9                   14:27:10   [{Origin: i} {Med: 0} {LocalPref: 100}]
	[charlesr@bgp-colo ~]$

From the point of view of the PFSense router, we see just the following line:

	flags: * = Valid, > = Selected, I = via IBGP, A = Announced, S = Stale
	origin: i = IGP, e = EGP, ? = Incomplete

	flags destination          gateway          lpref   med aspath origin
	I*>   172.18.255.1/32      172.18.129.8       100     0 i

As you can see from the output from asgard, the route heading to 172.18.129.8
has been selected as the best route. But what the other server is primary
server we want to use? MED to the rescue! Let's apply a MED value on
172.18.129.8. We do this through a route-map:

	router bgp 65301
	 bgp router-id 172.18.129.8
	 network 172.18.255.1/32 route-map MED
	 neighbor 172.18.129.2 remote-as 65301
	!
	route-map MED permit 1
	 set metric 160
	!

After clearing the BGP peerings, we can see the routes in GoBGP reflect the
changes we just made:

	   Network              Next Hop             AS_PATH  Age        Attrs
	*> 172.18.255.1/32      172.18.129.9                  14:24:27   [{Origin: i} {Med: 0} {LocalPref: 100}]
	*  172.18.255.1/32      172.18.129.8                  00:00:02   [{Origin: i} {Med: 160} {LocalPref: 100}]

And on asgard:

	flags: * = Valid, > = Selected, I = via IBGP, A = Announced, S = Stale
	origin: i = IGP, e = EGP, ? = Incomplete

	flags destination          gateway          lpref   med aspath origin
	I*>   172.18.255.1/32      172.18.129.9       100     0 i


And now all the traffic will head to 172.18.129.9.


