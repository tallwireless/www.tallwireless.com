+++
title = "Intro into Segment Routing"
draft = true
description = ""
categories = ["Networking","Routing"]
tags = ["nextgen","igb","mpls","intro","segment"]
comments = false
date = "2017-04-15T14:40:24-04:00"

+++

Recently, I have found myself exploring different kinds network architectures.
The interest in new architectures were driven by several factors at work. Most
of the use cases I have have been using to drive my research and interest have
been supporting wireless architectures, providing flexiblity in the core, and
next generation data center architectures.

A good chunk of the research I have been doing has been focused in MPLS, with
great interest in L3VPNs and EVPNs. Over the last couple of months, I have
been doing a fair amount of reading from the book [_MPLS in the SDN era_][0].
The book does a great job of covering a broad range of subjects matter
regarding MPLS, and covers the interoperablity between Juniper and Cisco.

One of the subjects in the book is covered briefly is the concept of segment
routing. This recently came up again in my weekly network architecture
meeting, so I went forth and did some more research on the subject matter.
This is probably the first in a long line of segment routing related posts as
I dive deeper into the subject matter.

## Overview

## Terminology
This section of terminology is based on the draft RFC ["Segment Routing
Architecture"][1].

**segment:** words

**segment ID (SID):** words

**segment list:** words

## Understanding Packet Flow
### Example Network
![Example Network](img/example_network.png)

[0]: https://www.amazon.com/MPLS-SDN-Era-Interoperable-Scenarios/dp/149190545X
[1]: https://datatracker.ietf.org/doc/draft-ietf-spring-segment-routing/
