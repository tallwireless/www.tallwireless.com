+++
tags = ["linux","networking","cli","routing"]
comments = false
draft = true
description = ""
categories = ["Linux","Networking","Routing"]
date = "2017-04-12T12:23:52-04:00"
title = "Multihomed Linux and L2 Traffic"
slug = "multihomed-linux-l2-traffic"

+++


I've been working with Aaron on the throughput issues with his cluster
of PerfSonar nodes. To refresh, he was getting 47Mbps of throughput on
IPv4 even though all of the nodes were plugged into the same L2 switch.

Because of another issue that I had uncovered (and clearly needs some
further investigation), the 47Mbps sounded awfully familiar to be, and
made be think that the traffic was heading up too the router, and then
back down.

Doing some troubleshooting with Aaron, we discovered that simple pings
between 165.123.50.179 and 165.123.50.180 resulted in something every
interesting. The destination MAC address on the packets was that of the
router, and not that of the other host. Puzzling!

My first instinct was there was a routing issue with in the box, but the
output looked fine:

    [root@nerd-lab-fw-01 ~]# ip route list
    165.123.50.176/29 dev eth1  proto kernel  scope link  src 165.123.50.179
    default via 165.123.50.177 dev eth1

Maybe it was learning the MAC addresses wrong? Nope:

    [root@nerd-lab-fw-01 ~]# arp -an
    ? (165.123.50.180) at 00:01:2e:70:f4:18 [ether] on eth1
    ? (165.123.50.178) at <incomplete> on eth1
    ? (165.123.50.177) at 00:24:38:ad:e7:00 [ether] on eth1

Where does IP route want send the traffic locally:
    
    [root@nerd-lab-fw-01 ~]# ip route get 162.123.50.180
    162.123.50.180 via 165.123.50.177 dev eth1  src 165.123.50.179
        cache  mtu 1500 hoplimit 64

Interesting...it wants to send it up to the gateway. I should note at
this point something about the output from the ip route command. The
following line is what tells the kernel to send traffic locally for
hosts in the same subnet:

    165.123.50.176/29 dev eth1  proto kernel  scope link  src 165.123.50.179

But the line is there! Why is it sending traffic to the gateway for L2
reachable hosts???

Because of other project, I was aware of the fact that multiple routing
tables can exist in a single box. So I looked at all the tables
available to me on the box:

    [root@nerd-lab-fw-01 ~]# ip -4 route list table all
    default via 165.123.50.177 dev eth1  table eth1_source_route
    165.123.50.176/29 dev eth1  proto kernel  scope link  src 165.123.50.179
    default via 165.123.50.177 dev eth1
    broadcast 127.255.255.255 dev lo  table local  proto kernel  scope link src 127.0.0.1
    broadcast 165.123.50.176 dev eth1  table local  proto kernel  scope link src 165.123.50.179
    local 165.123.50.179 dev eth1  table local  proto kernel  scope host src 165.123.50.179
    broadcast 127.0.0.0 dev lo  table local  proto kernel  scope link  src 127.0.0.1
    broadcast 165.123.50.183 dev eth1  table local  proto kernel  scope link src 165.123.50.179
    local 127.0.0.1 dev lo  table local  proto kernel  scope host  src 127.0.0.1
    local 127.0.0.0/8 dev lo  table local  proto kernel  scope host  src 127.0.0.1

It all looks fine to me. I did notice the table eth1_source_route had
been created, but I thought that might be an internal thing for handling
the src IP address stuff internal to the kernel. It's table was this:

    [root@nerd-lab-fw-01 ~]# ip route list table eth1_source_route
    default via 165.123.50.177 dev eth1

Looked fine to me! So, to the internet! While poking around, I found an
article on running a machine with multiple default gateways [0]. Reading
through it, it introduced me to the concept of 'ip rules'. Interesting,
what does that produce:

    [root@nerd-lab-fw-01 ~]# ip rule
    0:    from all lookup local
    200:    from 165.123.50.179 lookup eth1_source_route
    32766:    from all lookup main
    32767:    from all lookup default


AH HA! If you look at line 200, you can see that traffic FROM
165.123.50.179 is to use the table eth1_source_route, and the table
eth1_source_route is missing the route for handling local traffic
properly, so I added it to the table, and presto, we have gig test
results!

Essentially, traffic inbound and outbound from the host was being
handled by two different tables. Inbound being handled by the default
table, and outbound being handled by the eth1_source_route table.

I was curious about what making this horrible miss configuration. Diving
through the source code for PerfSonar, the table and such is set up in
the file /usr/lib/perfsonar/scripts/mod_interface_route. They are adding
the default gateway, but not adding the local L2 network route. I take
it they are making an assumption that one would not be running tests
between hosts on the same layer 2 network. I was going to make an
attempt to fix the code, but I would need to make a deeper dive to
better understand what is being pasted into the function doing the work,
and how to find out the subnet size.

Probably just better to open a ticket with ESNet about it.
