+++
date = "2017-04-11T23:12:07-04:00"
title = "Contact"
draft = false

+++

If you are looking to contact me, the best way is by
[email](mailto:charlesr@tallwireless.com). I also update my
[Twitter](https://www.twitter.com/tallwireless) from time to time.
