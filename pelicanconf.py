#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

import os

AUTHOR = "Charles Rumford"
SITENAME = "TallWireless"
SIDEBAR_DIGEST = "not just wireless anymore"
SITEURL = os.getenv("URL", "http://localhost:8000")

PATH = "content"

TIMEZONE = "America/New_York"
DEFAULT_DATE_FORMAT = "%Y/%m/%d %H:%M"

DEFAULT_LANG = "en"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DISPLAY_CATEGORIES_ON_MENU = False

THEME = "themes/pelican-blue"

DISPLAY_SUMMARY = True

DISPLAY_PAGES_ON_MENU = True

STATIC_CHECK_IF_MODIFIED = True

SLUGIFY_SOURCE = "basename"

SOCIAL = (
    ("linkedin", "https://www.linkedin.com/in/tallwireless"),
    ("github", "https://github.com/tallwireless"),
    ("twitter", "https://twitter.com/tallwireless"),
)

PAGE_URL = "{slug}/"
PAGE_SAVE_AS = "{slug}/index.html"
PAGE_ORDER_BY = "side_weight"

CATEGORY_URL = "category/{slug}/"
CATEGORY_SAVE_AS = "category/{slug}/index.html"

TAG_URL = "tag/{slug}/"
TAG_SAVE_AS = "tag/{slug}/index.html"

AUTHOR_URL = "author/{slug}/"
AUTHOR_SAVE_AS = "author/{slug}/index.html"

ARTICLE_URL = "posts/{date:%Y}/{date:%m}/{date:%d}/{slug}/"
ARTICLE_SAVE_AS = "posts/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html"

PLUGINS = ["plugins.page_hierarchy"]
LOAD_CONTENT_CACHE = False


MENUITEMS = [
    {"side_weight": -1000, "title": "Home", "url": ""},
    {"side_weight": 300, "title": "Talks", "url": "category/talks/"},
]
# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)

# Social widget
# SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

HOMEPAGE_TITLE = "Welcome"

HOMEPAGE_WELCOME = """
The site is meant to be a dumping ground for different thoughts and ideas
which run through my head. There are also some projects I have worked on.
Please feel free to dive in and see the wide array of stuff that interests me.
"""
